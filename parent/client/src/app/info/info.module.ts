import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { InfoRoutingModule } from './info.routing'

@NgModule({
  imports: [
    RouterModule,
    InfoRoutingModule
  ]
})

export class InfoModule {
  constructor () {
    console.log('InfoModule - constructor')
  }
}
