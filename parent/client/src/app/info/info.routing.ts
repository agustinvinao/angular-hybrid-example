import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { CommonModule } from '@angular/common'
import { InfoComponent } from './info.component';

const routes: Routes = [
  {
    path: 'info',
    component: InfoComponent,
    data: { name: 'info' }
  }
]

@NgModule({
  declarations: [ InfoComponent ],
  entryComponents: [ InfoComponent ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class InfoRoutingModule { }
