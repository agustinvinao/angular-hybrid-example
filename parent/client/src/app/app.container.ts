import { Component, OnInit } from '@angular/core'

export const SELECTOR = 'app-container'

@Component({
  selector: SELECTOR,
  template: `<p>container</p>`,
  styles: [`p { background-color: black;color: white; padding: 10px;}`]
})
export class AppContainer implements OnInit {
  constructor () {
    console.log('AppContainer - constructor')
  }

  ngOnInit () {
    console.log('AppContainer - onInit')
  }
}
