import { Component, OnInit } from '@angular/core'

export const ROOT_SELECTOR = 'app'

@Component({
  selector: ROOT_SELECTOR,
  template: `<p>PARENT - AppComponent</p>
    <ul>
      <li><a routerLink="/foo">foo</a></li>
      <li><a routerLink="/bar">bar</a></li>
      <li><a routerLink="/info">info</a></li>
      <li><a routerLink="/preloaded">preloaded</a></li>
      <li><a routerLink="/child">Child</a></li>
    </ul>
    <router-outlet></router-outlet>`,
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor () {
    console.log('AppModule - AppComponent')
  }

  ngOnInit () {
    console.log('Angular Initialization...')
  }
}
