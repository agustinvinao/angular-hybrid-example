import { Component, OnInit } from '@angular/core'

export const SELECTOR = 'foo-component'

@Component({
  selector: SELECTOR,
  template: `<p>FOO</p>`,
  styles: [`p { background-color: black;color: white; padding: 10px;}`]
})
export class FooComponent implements OnInit {
  constructor() {
    console.log('FooComponent - constructor')
  }

  ngOnInit () {
    console.log('FooComponent - onInit')
  }
}
