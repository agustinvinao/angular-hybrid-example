import { NgModule, Component } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { Routes, RouterModule } from '@angular/router'
import { CommonModule, APP_BASE_HREF } from '@angular/common'
import { AppContainer } from './app.container'
import { FooModule } from './foo/foo.module'
import { FooComponent } from './foo.component';
import { BarComponent } from './bar.component';

const routes: Routes = [
  {
    path: '',
    data: { name: 'parent' },
    children: [
      {
        path: 'foo',
        component: FooComponent,
        data: { name: 'foo' }
      },
      {
        path: 'bar',
        component: BarComponent,
        data: { name: 'bar' }
      },
      {
        path: 'info',
        loadChildren: './info/info.module#InfoModule'
      },
      {
        path: 'preloaded',
        loadChildren: () => FooModule
      },
      {
        path: 'child',
        loadChildren: () => new Promise((resolve, reject) => {
          import('@root/public/child/bundles/core-architecture-child.umd.js')
            .then((child) => resolve(child.AppModule))
            .catch(reject)
        })
      }
    ]
  }
]

@NgModule({
  declarations: [
    AppContainer,
    FooComponent,
    BarComponent
  ],
  entryComponents: [AppContainer],
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule.forRoot(routes),
    FooModule
  ],
  providers: [{ provide: APP_BASE_HREF, useValue: '/' }]
})
export class AppRoutingModule { }
