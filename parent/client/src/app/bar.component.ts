import { Component, OnInit } from '@angular/core'

export const SELECTOR = 'bar-component'

@Component({
  selector: SELECTOR,
  template: `<p>BAR</p>`,
  styles: [`p { background-color: black;color: white; padding: 10px;}`]
})
export class BarComponent implements OnInit {
  constructor() {
    console.log('BarComponent - constructor')
  }

  ngOnInit () {
    console.log('BarComponent - onInit')
  }
}
