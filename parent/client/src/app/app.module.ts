import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { AppComponent } from './app.component'
import { RouterModule } from '@angular/router'
import { AppRoutingModule } from './app.routing'

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent
  ],
  entryComponents: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule
  ]
})

export class AppModule {
  constructor () {
    console.log('AppModule')
  }
}
