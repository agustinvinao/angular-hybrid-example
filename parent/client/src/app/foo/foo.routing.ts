import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { CommonModule } from '@angular/common'
import { FooComponent } from './foo.component'

const routes: Routes = [
  {
    path: '',
    component: FooComponent,
    data: { name: 'foo' },
    children: [
      {
        path: 'foo-foo',
        component: FooComponent,
        data: { name: 'foo-foo' }
      },
      {
        path: 'foo-bar',
        component: FooComponent,
        data: { name: 'foo-bar' }
      }
    ]
  }
]

@NgModule({
  declarations: [ FooComponent ],
  entryComponents: [ FooComponent ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class FooRoutingModule { }
