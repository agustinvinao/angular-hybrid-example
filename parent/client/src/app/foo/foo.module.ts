import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { FooRoutingModule } from './foo.routing'

@NgModule({
  imports: [
    RouterModule,
    FooRoutingModule
  ]
})

export class FooModule {
  constructor () {
    console.log('FooModule - constructor')
  }
}
