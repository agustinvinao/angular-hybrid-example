import { Component } from '@angular/core'

@Component({
  selector: 'foo-info',
  template: '<p>foo</p>'
})
export class FooComponent {
  constructor () {
    console.log('FooComponent')
  }
}
