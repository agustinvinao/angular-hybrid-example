import { filter } from 'rxjs/operators';
import { CommonModule } from '@angular/common';
import { Component, NgModule } from '@angular/core';
import { Router, ActivationEnd, ActivatedRoute, RouterModule } from '@angular/router';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/** @type {?} */
const ROOT_SELECTOR = 'child-app';
class AppComponent {
    /**
     * @param {?} router
     */
    constructor(router) {
        this.router = router;
        console.log('AppModule - AppComponent');
        router.events.pipe(filter((event) => event instanceof ActivationEnd)).subscribe((events) => console.log('%cCurrentRouteData: ', 'background-color:green;color:white', events.toString()));
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        console.log('Angular Initialization...');
    }
}
AppComponent.decorators = [
    { type: Component, args: [{
                selector: ROOT_SELECTOR,
                template: `<p>CHILD - AppComponent</p>
    <router-outlet></router-outlet>
  `,
                styles: [":host{background-color:#000;color:#fff}:host p{background-color:#000;color:#fff;padding:10px}"]
            }] }
];
/** @nocollapse */
AppComponent.ctorParameters = () => [
    { type: Router }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/** @type {?} */
const SELECTOR = 'app-container';
class AppContainer {
    /**
     * @param {?} activatedroute
     */
    constructor(activatedroute) {
        this.activatedroute = activatedroute;
        console.log('CHILD - AppContainer - constructor');
        activatedroute.data.subscribe(({ name }) => this.name = name);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        console.log('AppContainer - onInit');
    }
}
AppContainer.decorators = [
    { type: Component, args: [{
                selector: SELECTOR,
                template: `
  <p>CHILD - container</p>
  <ul>
    <li><a [routerLink]="['../foo']">foo</a></li>
    <li><a [routerLink]="['../bar']">bar</a></li>
  </ul>
  <div [className]="name">route: {{name}}</div>`,
                styles: [`
    .foo {
      background-color: red;
      color: white;
      padding: 10px;
    }
    .bar {
      background-color: green;
      color: white;
      padding: 10px;
    }`]
            }] }
];
/** @nocollapse */
AppContainer.ctorParameters = () => [
    { type: ActivatedRoute }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
const ɵ0 = {
    name: 'foo'
}, ɵ1 = {
    name: 'bar'
};
/** @type {?} */
const routes = [
    {
        path: '',
        redirectTo: 'foo'
    },
    {
        path: 'foo',
        component: AppContainer,
        data: ɵ0
    },
    {
        path: 'bar',
        component: AppContainer,
        data: ɵ1
    }
];
class AppRoutingModule {
}
AppRoutingModule.decorators = [
    { type: NgModule, args: [{
                declarations: [AppContainer],
                entryComponents: [AppContainer],
                imports: [
                    CommonModule,
                    // RouterModule.forRoot(routes, { useHash: true })
                    RouterModule.forChild(routes)
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class AppModule {
    constructor() {
        console.log('CHILD - AppModule - constructor');
    }
}
AppModule.decorators = [
    { type: NgModule, args: [{
                bootstrap: [AppComponent],
                declarations: [
                    AppComponent
                ],
                imports: [
                    RouterModule,
                    CommonModule,
                    AppRoutingModule
                ]
            },] }
];
/** @nocollapse */
AppModule.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

export { AppModule, AppComponent as ɵb, ROOT_SELECTOR as ɵa, AppContainer as ɵe, SELECTOR as ɵd, AppRoutingModule as ɵc };

//# sourceMappingURL=core-architecture-child.js.map