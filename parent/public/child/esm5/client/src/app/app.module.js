/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
var AppModule = /** @class */ (function () {
    function AppModule() {
        console.log('CHILD - AppModule - constructor');
    }
    AppModule.decorators = [
        { type: NgModule, args: [{
                    bootstrap: [AppComponent],
                    declarations: [
                        AppComponent
                    ],
                    imports: [
                        RouterModule,
                        CommonModule,
                        AppRoutingModule
                    ]
                },] }
    ];
    /** @nocollapse */
    AppModule.ctorParameters = function () { return []; };
    return AppModule;
}());
export { AppModule };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtYXJjaGl0ZWN0dXJlLWNoaWxkLyIsInNvdXJjZXMiOlsiY2xpZW50L3NyYy9hcHAvYXBwLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQTtBQUN4QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUE7QUFDOUMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBRTlDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQUM5QyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxlQUFlLENBQUE7O0lBZ0I5QztRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUNBQWlDLENBQUMsQ0FBQTtLQUMvQzs7Z0JBaEJGLFFBQVEsU0FBQztvQkFDUixTQUFTLEVBQUUsQ0FBQyxZQUFZLENBQUM7b0JBQ3pCLFlBQVksRUFBRTt3QkFDWixZQUFZO3FCQUNiO29CQUNELE9BQU8sRUFBRTt3QkFFUCxZQUFZO3dCQUNaLFlBQVk7d0JBQ1osZ0JBQWdCO3FCQUNqQjtpQkFDRjs7OztvQkFsQkQ7O1NBb0JhLFNBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInXG4vLyBpbXBvcnQgeyBCcm93c2VyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3NlcidcbmltcG9ydCB7IEFwcENvbXBvbmVudCB9IGZyb20gJy4vYXBwLmNvbXBvbmVudCdcbmltcG9ydCB7IEFwcFJvdXRpbmdNb2R1bGUgfSBmcm9tICcuL2FwcC5yb3V0aW5nJ1xuXG5ATmdNb2R1bGUoe1xuICBib290c3RyYXA6IFtBcHBDb21wb25lbnRdLFxuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBBcHBDb21wb25lbnRcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIC8vIEJyb3dzZXJNb2R1bGUsXG4gICAgUm91dGVyTW9kdWxlLFxuICAgIENvbW1vbk1vZHVsZSxcbiAgICBBcHBSb3V0aW5nTW9kdWxlXG4gIF1cbn0pXG5cbmV4cG9ydCBjbGFzcyBBcHBNb2R1bGUge1xuICBjb25zdHJ1Y3RvciAoKSB7XG4gICAgY29uc29sZS5sb2coJ0NISUxEIC0gQXBwTW9kdWxlIC0gY29uc3RydWN0b3InKVxuICB9XG59XG4iXX0=