/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Router, ActivationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
/** @type {?} */
export var ROOT_SELECTOR = 'child-app';
var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
        console.log('AppModule - AppComponent');
        router.events.pipe(filter(function (event) { return event instanceof ActivationEnd; })).subscribe(function (events) { return console.log('%cCurrentRouteData: ', 'background-color:green;color:white', events.toString()); });
    }
    /**
     * @return {?}
     */
    AppComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        console.log('Angular Initialization...');
    };
    AppComponent.decorators = [
        { type: Component, args: [{
                    selector: ROOT_SELECTOR,
                    template: "<p>CHILD - AppComponent</p>\n    <router-outlet></router-outlet>\n  ",
                    styles: [":host{background-color:#000;color:#fff}:host p{background-color:#000;color:#fff;padding:10px}"]
                }] }
    ];
    /** @nocollapse */
    AppComponent.ctorParameters = function () { return [
        { type: Router }
    ]; };
    return AppComponent;
}());
export { AppComponent };
if (false) {
    /** @type {?} */
    AppComponent.prototype.routeConfig;
    /** @type {?} */
    AppComponent.prototype.router;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtYXJjaGl0ZWN0dXJlLWNoaWxkLyIsInNvdXJjZXMiOlsiY2xpZW50L3NyYy9hcHAvYXBwLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQTtBQUNqRCxPQUFPLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQ3ZELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTs7QUFDdkMsV0FBYSxhQUFhLEdBQUcsV0FBVyxDQUFBOztJQVd0QyxzQkFDVTtRQUFBLFdBQU0sR0FBTixNQUFNO1FBRWQsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsQ0FBQyxDQUFBO1FBQ3ZDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUNoQixNQUFNLENBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFLLFlBQVksYUFBYSxFQUE5QixDQUE4QixDQUFDLENBQ2xELENBQUMsU0FBUyxDQUNULFVBQUMsTUFBcUIsSUFBSyxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLEVBQUUsb0NBQW9DLEVBQUUsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLEVBQTVGLENBQTRGLENBQ3hILENBQUE7S0FDRjs7OztJQUVELCtCQUFROzs7SUFBUjtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLENBQUMsQ0FBQTtLQUN6Qzs7Z0JBdEJGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsYUFBYTtvQkFDdkIsUUFBUSxFQUFFLHNFQUVUOztpQkFFRjs7OztnQkFWUSxNQUFNOzt1QkFEZjs7U0FZYSxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0aW9uRW5kIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJ1xuaW1wb3J0IHsgZmlsdGVyIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnXG5leHBvcnQgY29uc3QgUk9PVF9TRUxFQ1RPUiA9ICdjaGlsZC1hcHAnXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogUk9PVF9TRUxFQ1RPUixcbiAgdGVtcGxhdGU6IGA8cD5DSElMRCAtIEFwcENvbXBvbmVudDwvcD5cbiAgICA8cm91dGVyLW91dGxldD48L3JvdXRlci1vdXRsZXQ+XG4gIGAsXG4gIHN0eWxlVXJsczogWycuL2FwcC5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEFwcENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIHByaXZhdGUgcm91dGVDb25maWc6IGFueVxuICBjb25zdHJ1Y3RvciAoXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlclxuICApIHtcbiAgICBjb25zb2xlLmxvZygnQXBwTW9kdWxlIC0gQXBwQ29tcG9uZW50JylcbiAgICByb3V0ZXIuZXZlbnRzLnBpcGUoXG4gICAgICBmaWx0ZXIoKGV2ZW50KSA9PiBldmVudCBpbnN0YW5jZW9mIEFjdGl2YXRpb25FbmQpXG4gICAgKS5zdWJzY3JpYmUoXG4gICAgICAoZXZlbnRzOiBBY3RpdmF0aW9uRW5kKSA9PiBjb25zb2xlLmxvZygnJWNDdXJyZW50Um91dGVEYXRhOiAnLCAnYmFja2dyb3VuZC1jb2xvcjpncmVlbjtjb2xvcjp3aGl0ZScsIGV2ZW50cy50b1N0cmluZygpKVxuICAgIClcbiAgfVxuXG4gIG5nT25Jbml0ICgpIHtcbiAgICBjb25zb2xlLmxvZygnQW5ndWxhciBJbml0aWFsaXphdGlvbi4uLicpXG4gIH1cbn1cbiJdfQ==