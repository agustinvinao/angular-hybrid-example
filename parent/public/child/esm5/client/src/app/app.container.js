/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
/** @type {?} */
export var SELECTOR = 'app-container';
var AppContainer = /** @class */ (function () {
    function AppContainer(activatedroute) {
        var _this = this;
        this.activatedroute = activatedroute;
        console.log('CHILD - AppContainer - constructor');
        activatedroute.data.subscribe(function (_a) {
            var name = _a.name;
            return _this.name = name;
        });
    }
    /**
     * @return {?}
     */
    AppContainer.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        console.log('AppContainer - onInit');
    };
    AppContainer.decorators = [
        { type: Component, args: [{
                    selector: SELECTOR,
                    template: "\n  <p>CHILD - container</p>\n  <ul>\n    <li><a [routerLink]=\"['../foo']\">foo</a></li>\n    <li><a [routerLink]=\"['../bar']\">bar</a></li>\n  </ul>\n  <div [className]=\"name\">route: {{name}}</div>",
                    styles: ["\n    .foo {\n      background-color: red;\n      color: white;\n      padding: 10px;\n    }\n    .bar {\n      background-color: green;\n      color: white;\n      padding: 10px;\n    }"]
                }] }
    ];
    /** @nocollapse */
    AppContainer.ctorParameters = function () { return [
        { type: ActivatedRoute }
    ]; };
    return AppContainer;
}());
export { AppContainer };
if (false) {
    /** @type {?} */
    AppContainer.prototype.name;
    /** @type {?} */
    AppContainer.prototype.activatedroute;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbnRhaW5lci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtYXJjaGl0ZWN0dXJlLWNoaWxkLyIsInNvdXJjZXMiOlsiY2xpZW50L3NyYy9hcHAvYXBwLmNvbnRhaW5lci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQTtBQUNqRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0saUJBQWlCLENBQUE7O0FBR2hELFdBQWEsUUFBUSxHQUFHLGVBQWUsQ0FBQTs7SUF5QnJDLHNCQUNVO1FBRFYsaUJBT0M7UUFOUyxtQkFBYyxHQUFkLGNBQWM7UUFFdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFBO1FBQ2pELGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUMzQixVQUFDLEVBQVE7Z0JBQU4sY0FBSTtZQUFPLE9BQUEsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJO1FBQWhCLENBQWdCLENBQy9CLENBQUE7S0FDRjs7OztJQUVELCtCQUFROzs7SUFBUjtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQTtLQUNyQzs7Z0JBbENGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsUUFBUTtvQkFDbEIsUUFBUSxFQUFFLDRNQU1vQzs2QkFDckMsNExBVUw7aUJBQ0w7Ozs7Z0JBekJRLGNBQWM7O3VCQUR2Qjs7U0EyQmEsWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJ1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnXG5cbmV4cG9ydCBjb25zdCBTRUxFQ1RPUiA9ICdhcHAtY29udGFpbmVyJ1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFNFTEVDVE9SLFxuICB0ZW1wbGF0ZTogYFxuICA8cD5DSElMRCAtIGNvbnRhaW5lcjwvcD5cbiAgPHVsPlxuICAgIDxsaT48YSBbcm91dGVyTGlua109XCJbJy4uL2ZvbyddXCI+Zm9vPC9hPjwvbGk+XG4gICAgPGxpPjxhIFtyb3V0ZXJMaW5rXT1cIlsnLi4vYmFyJ11cIj5iYXI8L2E+PC9saT5cbiAgPC91bD5cbiAgPGRpdiBbY2xhc3NOYW1lXT1cIm5hbWVcIj5yb3V0ZToge3tuYW1lfX08L2Rpdj5gLFxuICBzdHlsZXM6IFtgXG4gICAgLmZvbyB7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG4gICAgICBjb2xvcjogd2hpdGU7XG4gICAgICBwYWRkaW5nOiAxMHB4O1xuICAgIH1cbiAgICAuYmFyIHtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IGdyZWVuO1xuICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgcGFkZGluZzogMTBweDtcbiAgICB9YF1cbn0pXG5leHBvcnQgY2xhc3MgQXBwQ29udGFpbmVyIGltcGxlbWVudHMgT25Jbml0IHtcbiAgcHVibGljIG5hbWU6IE9ic2VydmFibGU8c3RyaW5nPlxuICBjb25zdHJ1Y3RvciAoXG4gICAgcHJpdmF0ZSBhY3RpdmF0ZWRyb3V0ZTogQWN0aXZhdGVkUm91dGVcbiAgKSB7XG4gICAgY29uc29sZS5sb2coJ0NISUxEIC0gQXBwQ29udGFpbmVyIC0gY29uc3RydWN0b3InKVxuICAgIGFjdGl2YXRlZHJvdXRlLmRhdGEuc3Vic2NyaWJlKFxuICAgICAgKHsgbmFtZSB9KSA9PiB0aGlzLm5hbWUgPSBuYW1lXG4gICAgKVxuICB9XG5cbiAgbmdPbkluaXQgKCkge1xuICAgIGNvbnNvbGUubG9nKCdBcHBDb250YWluZXIgLSBvbkluaXQnKVxuICB9XG59XG4iXX0=