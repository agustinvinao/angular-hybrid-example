/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppContainer } from './app.container';
var ɵ0 = {
    name: 'foo'
}, ɵ1 = {
    name: 'bar'
};
/** @type {?} */
var routes = [
    {
        path: '',
        redirectTo: 'foo'
    },
    {
        path: 'foo',
        component: AppContainer,
        data: ɵ0
    },
    {
        path: 'bar',
        component: AppContainer,
        data: ɵ1
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [AppContainer],
                    entryComponents: [AppContainer],
                    imports: [
                        CommonModule,
                        // RouterModule.forRoot(routes, { useHash: true })
                        RouterModule.forChild(routes)
                    ]
                },] }
    ];
    return AppRoutingModule;
}());
export { AppRoutingModule };
export { ɵ0, ɵ1 };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLnJvdXRpbmcuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb3JlLWFyY2hpdGVjdHVyZS1jaGlsZC8iLCJzb3VyY2VzIjpbImNsaWVudC9zcmMvYXBwL2FwcC5yb3V0aW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBQ3hDLE9BQU8sRUFBVSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQUN0RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUE7QUFDOUMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO1NBVXBDO0lBQ0osSUFBSSxFQUFFLEtBQUs7Q0FDWixPQUtLO0lBQ0osSUFBSSxFQUFFLEtBQUs7Q0FDWjs7QUFqQkwsSUFBTSxNQUFNLEdBQVc7SUFDckI7UUFDRSxJQUFJLEVBQUUsRUFBRTtRQUNSLFVBQVUsRUFBRSxLQUFLO0tBQ2xCO0lBQ0Q7UUFDRSxJQUFJLEVBQUUsS0FBSztRQUNYLFNBQVMsRUFBRSxZQUFZO1FBQ3ZCLElBQUksSUFFSDtLQUNGO0lBQ0Q7UUFDRSxJQUFJLEVBQUUsS0FBSztRQUNYLFNBQVMsRUFBRSxZQUFZO1FBQ3ZCLElBQUksSUFFSDtLQUNGO0NBQ0YsQ0FBQTs7Ozs7Z0JBRUEsUUFBUSxTQUFDO29CQUNSLFlBQVksRUFBRSxDQUFDLFlBQVksQ0FBQztvQkFDNUIsZUFBZSxFQUFFLENBQUMsWUFBWSxDQUFDO29CQUMvQixPQUFPLEVBQUU7d0JBQ1AsWUFBWTs7d0JBRVosWUFBWSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7cUJBQzlCO2lCQUNGOzsyQkFsQ0Q7O1NBbUNhLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcbmltcG9ydCB7IFJvdXRlcywgUm91dGVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJ1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJ1xuaW1wb3J0IHsgQXBwQ29udGFpbmVyIH0gZnJvbSAnLi9hcHAuY29udGFpbmVyJ1xuXG5jb25zdCByb3V0ZXM6IFJvdXRlcyA9IFtcbiAge1xuICAgIHBhdGg6ICcnLFxuICAgIHJlZGlyZWN0VG86ICdmb28nXG4gIH0sXG4gIHtcbiAgICBwYXRoOiAnZm9vJyxcbiAgICBjb21wb25lbnQ6IEFwcENvbnRhaW5lcixcbiAgICBkYXRhOiB7XG4gICAgICBuYW1lOiAnZm9vJ1xuICAgIH1cbiAgfSxcbiAge1xuICAgIHBhdGg6ICdiYXInLFxuICAgIGNvbXBvbmVudDogQXBwQ29udGFpbmVyLFxuICAgIGRhdGE6IHtcbiAgICAgIG5hbWU6ICdiYXInXG4gICAgfVxuICB9XG5dXG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW0FwcENvbnRhaW5lcl0sXG4gIGVudHJ5Q29tcG9uZW50czogW0FwcENvbnRhaW5lcl0sXG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgLy8gUm91dGVyTW9kdWxlLmZvclJvb3Qocm91dGVzLCB7IHVzZUhhc2g6IHRydWUgfSlcbiAgICBSb3V0ZXJNb2R1bGUuZm9yQ2hpbGQocm91dGVzKVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIEFwcFJvdXRpbmdNb2R1bGUgeyB9XG4iXX0=