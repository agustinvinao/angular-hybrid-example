(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('rxjs/operators'), require('@angular/common'), require('@angular/core'), require('@angular/router')) :
    typeof define === 'function' && define.amd ? define('core-architecture-child', ['exports', 'rxjs/operators', '@angular/common', '@angular/core', '@angular/router'], factory) :
    (factory((global['core-architecture-child'] = {}),global.rxjs.operators,global.ng.common,global.ng.core,global.ng.router));
}(this, (function (exports,operators,common,core,router) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    /** @type {?} */
    var ROOT_SELECTOR = 'child-app';
    var AppComponent = /** @class */ (function () {
        function AppComponent(router$$1) {
            this.router = router$$1;
            console.log('AppModule - AppComponent');
            router$$1.events.pipe(operators.filter(function (event) { return event instanceof router.ActivationEnd; })).subscribe(function (events) { return console.log('%cCurrentRouteData: ', 'background-color:green;color:white', events.toString()); });
        }
        /**
         * @return {?}
         */
        AppComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                console.log('Angular Initialization...');
            };
        AppComponent.decorators = [
            { type: core.Component, args: [{
                        selector: ROOT_SELECTOR,
                        template: "<p>CHILD - AppComponent</p>\n    <router-outlet></router-outlet>\n  ",
                        styles: [":host{background-color:#000;color:#fff}:host p{background-color:#000;color:#fff;padding:10px}"]
                    }] }
        ];
        /** @nocollapse */
        AppComponent.ctorParameters = function () {
            return [
                { type: router.Router }
            ];
        };
        return AppComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    /** @type {?} */
    var SELECTOR = 'app-container';
    var AppContainer = /** @class */ (function () {
        function AppContainer(activatedroute) {
            var _this = this;
            this.activatedroute = activatedroute;
            console.log('CHILD - AppContainer - constructor');
            activatedroute.data.subscribe(function (_a) {
                var name = _a.name;
                return _this.name = name;
            });
        }
        /**
         * @return {?}
         */
        AppContainer.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                console.log('AppContainer - onInit');
            };
        AppContainer.decorators = [
            { type: core.Component, args: [{
                        selector: SELECTOR,
                        template: "\n  <p>CHILD - container</p>\n  <ul>\n    <li><a [routerLink]=\"['../foo']\">foo</a></li>\n    <li><a [routerLink]=\"['../bar']\">bar</a></li>\n  </ul>\n  <div [className]=\"name\">route: {{name}}</div>",
                        styles: ["\n    .foo {\n      background-color: red;\n      color: white;\n      padding: 10px;\n    }\n    .bar {\n      background-color: green;\n      color: white;\n      padding: 10px;\n    }"]
                    }] }
        ];
        /** @nocollapse */
        AppContainer.ctorParameters = function () {
            return [
                { type: router.ActivatedRoute }
            ];
        };
        return AppContainer;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var ɵ0 = {
        name: 'foo'
    }, ɵ1 = {
        name: 'bar'
    };
    /** @type {?} */
    var routes = [
        {
            path: '',
            redirectTo: 'foo'
        },
        {
            path: 'foo',
            component: AppContainer,
            data: ɵ0
        },
        {
            path: 'bar',
            component: AppContainer,
            data: ɵ1
        }
    ];
    var AppRoutingModule = /** @class */ (function () {
        function AppRoutingModule() {
        }
        AppRoutingModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [AppContainer],
                        entryComponents: [AppContainer],
                        imports: [
                            common.CommonModule,
                            // RouterModule.forRoot(routes, { useHash: true })
                            router.RouterModule.forChild(routes)
                        ]
                    },] }
        ];
        return AppRoutingModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var AppModule = /** @class */ (function () {
        function AppModule() {
            console.log('CHILD - AppModule - constructor');
        }
        AppModule.decorators = [
            { type: core.NgModule, args: [{
                        bootstrap: [AppComponent],
                        declarations: [
                            AppComponent
                        ],
                        imports: [
                            router.RouterModule,
                            common.CommonModule,
                            AppRoutingModule
                        ]
                    },] }
        ];
        /** @nocollapse */
        AppModule.ctorParameters = function () { return []; };
        return AppModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    exports.AppModule = AppModule;
    exports.ɵb = AppComponent;
    exports.ɵa = ROOT_SELECTOR;
    exports.ɵe = AppContainer;
    exports.ɵd = SELECTOR;
    exports.ɵc = AppRoutingModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=core-architecture-child.umd.js.map