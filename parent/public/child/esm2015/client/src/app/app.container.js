/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
/** @type {?} */
export const SELECTOR = 'app-container';
export class AppContainer {
    /**
     * @param {?} activatedroute
     */
    constructor(activatedroute) {
        this.activatedroute = activatedroute;
        console.log('CHILD - AppContainer - constructor');
        activatedroute.data.subscribe(({ name }) => this.name = name);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        console.log('AppContainer - onInit');
    }
}
AppContainer.decorators = [
    { type: Component, args: [{
                selector: SELECTOR,
                template: `
  <p>CHILD - container</p>
  <ul>
    <li><a [routerLink]="['../foo']">foo</a></li>
    <li><a [routerLink]="['../bar']">bar</a></li>
  </ul>
  <div [className]="name">route: {{name}}</div>`,
                styles: [`
    .foo {
      background-color: red;
      color: white;
      padding: 10px;
    }
    .bar {
      background-color: green;
      color: white;
      padding: 10px;
    }`]
            }] }
];
/** @nocollapse */
AppContainer.ctorParameters = () => [
    { type: ActivatedRoute }
];
if (false) {
    /** @type {?} */
    AppContainer.prototype.name;
    /** @type {?} */
    AppContainer.prototype.activatedroute;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbnRhaW5lci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtYXJjaGl0ZWN0dXJlLWNoaWxkLyIsInNvdXJjZXMiOlsiY2xpZW50L3NyYy9hcHAvYXBwLmNvbnRhaW5lci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQTtBQUNqRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0saUJBQWlCLENBQUE7O0FBR2hELGFBQWEsUUFBUSxHQUFHLGVBQWUsQ0FBQTtBQXVCdkMsTUFBTTs7OztJQUVKLFlBQ1U7UUFBQSxtQkFBYyxHQUFkLGNBQWM7UUFFdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFBO1FBQ2pELGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUMzQixDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUMvQixDQUFBO0tBQ0Y7Ozs7SUFFRCxRQUFRO1FBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFBO0tBQ3JDOzs7WUFsQ0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxRQUFRO2dCQUNsQixRQUFRLEVBQUU7Ozs7OztnREFNb0M7eUJBQ3JDOzs7Ozs7Ozs7O01BVUw7YUFDTDs7OztZQXpCUSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcydcblxuZXhwb3J0IGNvbnN0IFNFTEVDVE9SID0gJ2FwcC1jb250YWluZXInXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogU0VMRUNUT1IsXG4gIHRlbXBsYXRlOiBgXG4gIDxwPkNISUxEIC0gY29udGFpbmVyPC9wPlxuICA8dWw+XG4gICAgPGxpPjxhIFtyb3V0ZXJMaW5rXT1cIlsnLi4vZm9vJ11cIj5mb288L2E+PC9saT5cbiAgICA8bGk+PGEgW3JvdXRlckxpbmtdPVwiWycuLi9iYXInXVwiPmJhcjwvYT48L2xpPlxuICA8L3VsPlxuICA8ZGl2IFtjbGFzc05hbWVdPVwibmFtZVwiPnJvdXRlOiB7e25hbWV9fTwvZGl2PmAsXG4gIHN0eWxlczogW2BcbiAgICAuZm9vIHtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbiAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgfVxuICAgIC5iYXIge1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogZ3JlZW47XG4gICAgICBjb2xvcjogd2hpdGU7XG4gICAgICBwYWRkaW5nOiAxMHB4O1xuICAgIH1gXVxufSlcbmV4cG9ydCBjbGFzcyBBcHBDb250YWluZXIgaW1wbGVtZW50cyBPbkluaXQge1xuICBwdWJsaWMgbmFtZTogT2JzZXJ2YWJsZTxzdHJpbmc+XG4gIGNvbnN0cnVjdG9yIChcbiAgICBwcml2YXRlIGFjdGl2YXRlZHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVxuICApIHtcbiAgICBjb25zb2xlLmxvZygnQ0hJTEQgLSBBcHBDb250YWluZXIgLSBjb25zdHJ1Y3RvcicpXG4gICAgYWN0aXZhdGVkcm91dGUuZGF0YS5zdWJzY3JpYmUoXG4gICAgICAoeyBuYW1lIH0pID0+IHRoaXMubmFtZSA9IG5hbWVcbiAgICApXG4gIH1cblxuICBuZ09uSW5pdCAoKSB7XG4gICAgY29uc29sZS5sb2coJ0FwcENvbnRhaW5lciAtIG9uSW5pdCcpXG4gIH1cbn1cbiJdfQ==