/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppContainer } from './app.container';
const ɵ0 = {
    name: 'foo'
}, ɵ1 = {
    name: 'bar'
};
/** @type {?} */
const routes = [
    {
        path: '',
        redirectTo: 'foo'
    },
    {
        path: 'foo',
        component: AppContainer,
        data: ɵ0
    },
    {
        path: 'bar',
        component: AppContainer,
        data: ɵ1
    }
];
export class AppRoutingModule {
}
AppRoutingModule.decorators = [
    { type: NgModule, args: [{
                declarations: [AppContainer],
                entryComponents: [AppContainer],
                imports: [
                    CommonModule,
                    // RouterModule.forRoot(routes, { useHash: true })
                    RouterModule.forChild(routes)
                ]
            },] }
];
export { ɵ0, ɵ1 };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLnJvdXRpbmcuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb3JlLWFyY2hpdGVjdHVyZS1jaGlsZC8iLCJzb3VyY2VzIjpbImNsaWVudC9zcmMvYXBwL2FwcC5yb3V0aW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBQ3hDLE9BQU8sRUFBVSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQUN0RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUE7QUFDOUMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO1dBVXBDO0lBQ0osSUFBSSxFQUFFLEtBQUs7Q0FDWixPQUtLO0lBQ0osSUFBSSxFQUFFLEtBQUs7Q0FDWjs7QUFqQkwsTUFBTSxNQUFNLEdBQVc7SUFDckI7UUFDRSxJQUFJLEVBQUUsRUFBRTtRQUNSLFVBQVUsRUFBRSxLQUFLO0tBQ2xCO0lBQ0Q7UUFDRSxJQUFJLEVBQUUsS0FBSztRQUNYLFNBQVMsRUFBRSxZQUFZO1FBQ3ZCLElBQUksSUFFSDtLQUNGO0lBQ0Q7UUFDRSxJQUFJLEVBQUUsS0FBSztRQUNYLFNBQVMsRUFBRSxZQUFZO1FBQ3ZCLElBQUksSUFFSDtLQUNGO0NBQ0YsQ0FBQTtBQVdELE1BQU07OztZQVRMLFFBQVEsU0FBQztnQkFDUixZQUFZLEVBQUUsQ0FBQyxZQUFZLENBQUM7Z0JBQzVCLGVBQWUsRUFBRSxDQUFDLFlBQVksQ0FBQztnQkFDL0IsT0FBTyxFQUFFO29CQUNQLFlBQVk7O29CQUVaLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO2lCQUM5QjthQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xuaW1wb3J0IHsgUm91dGVzLCBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nXG5pbXBvcnQgeyBBcHBDb250YWluZXIgfSBmcm9tICcuL2FwcC5jb250YWluZXInXG5cbmNvbnN0IHJvdXRlczogUm91dGVzID0gW1xuICB7XG4gICAgcGF0aDogJycsXG4gICAgcmVkaXJlY3RUbzogJ2ZvbydcbiAgfSxcbiAge1xuICAgIHBhdGg6ICdmb28nLFxuICAgIGNvbXBvbmVudDogQXBwQ29udGFpbmVyLFxuICAgIGRhdGE6IHtcbiAgICAgIG5hbWU6ICdmb28nXG4gICAgfVxuICB9LFxuICB7XG4gICAgcGF0aDogJ2JhcicsXG4gICAgY29tcG9uZW50OiBBcHBDb250YWluZXIsXG4gICAgZGF0YToge1xuICAgICAgbmFtZTogJ2JhcidcbiAgICB9XG4gIH1cbl1cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbQXBwQ29udGFpbmVyXSxcbiAgZW50cnlDb21wb25lbnRzOiBbQXBwQ29udGFpbmVyXSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICAvLyBSb3V0ZXJNb2R1bGUuZm9yUm9vdChyb3V0ZXMsIHsgdXNlSGFzaDogdHJ1ZSB9KVxuICAgIFJvdXRlck1vZHVsZS5mb3JDaGlsZChyb3V0ZXMpXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgQXBwUm91dGluZ01vZHVsZSB7IH1cbiJdfQ==