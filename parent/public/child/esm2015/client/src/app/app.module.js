/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
export class AppModule {
    constructor() {
        console.log('CHILD - AppModule - constructor');
    }
}
AppModule.decorators = [
    { type: NgModule, args: [{
                bootstrap: [AppComponent],
                declarations: [
                    AppComponent
                ],
                imports: [
                    RouterModule,
                    CommonModule,
                    AppRoutingModule
                ]
            },] }
];
/** @nocollapse */
AppModule.ctorParameters = () => [];

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtYXJjaGl0ZWN0dXJlLWNoaWxkLyIsInNvdXJjZXMiOlsiY2xpZW50L3NyYy9hcHAvYXBwLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQTtBQUN4QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUE7QUFDOUMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBRTlDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQUM5QyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFlaEQsTUFBTTtJQUNKO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFBO0tBQy9DOzs7WUFoQkYsUUFBUSxTQUFDO2dCQUNSLFNBQVMsRUFBRSxDQUFDLFlBQVksQ0FBQztnQkFDekIsWUFBWSxFQUFFO29CQUNaLFlBQVk7aUJBQ2I7Z0JBQ0QsT0FBTyxFQUFFO29CQUVQLFlBQVk7b0JBQ1osWUFBWTtvQkFDWixnQkFBZ0I7aUJBQ2pCO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInXG4vLyBpbXBvcnQgeyBCcm93c2VyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3NlcidcbmltcG9ydCB7IEFwcENvbXBvbmVudCB9IGZyb20gJy4vYXBwLmNvbXBvbmVudCdcbmltcG9ydCB7IEFwcFJvdXRpbmdNb2R1bGUgfSBmcm9tICcuL2FwcC5yb3V0aW5nJ1xuXG5ATmdNb2R1bGUoe1xuICBib290c3RyYXA6IFtBcHBDb21wb25lbnRdLFxuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBBcHBDb21wb25lbnRcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIC8vIEJyb3dzZXJNb2R1bGUsXG4gICAgUm91dGVyTW9kdWxlLFxuICAgIENvbW1vbk1vZHVsZSxcbiAgICBBcHBSb3V0aW5nTW9kdWxlXG4gIF1cbn0pXG5cbmV4cG9ydCBjbGFzcyBBcHBNb2R1bGUge1xuICBjb25zdHJ1Y3RvciAoKSB7XG4gICAgY29uc29sZS5sb2coJ0NISUxEIC0gQXBwTW9kdWxlIC0gY29uc3RydWN0b3InKVxuICB9XG59XG4iXX0=