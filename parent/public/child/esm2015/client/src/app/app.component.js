/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Router, ActivationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
/** @type {?} */
export const ROOT_SELECTOR = 'child-app';
export class AppComponent {
    /**
     * @param {?} router
     */
    constructor(router) {
        this.router = router;
        console.log('AppModule - AppComponent');
        router.events.pipe(filter((event) => event instanceof ActivationEnd)).subscribe((events) => console.log('%cCurrentRouteData: ', 'background-color:green;color:white', events.toString()));
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        console.log('Angular Initialization...');
    }
}
AppComponent.decorators = [
    { type: Component, args: [{
                selector: ROOT_SELECTOR,
                template: `<p>CHILD - AppComponent</p>
    <router-outlet></router-outlet>
  `,
                styles: [":host{background-color:#000;color:#fff}:host p{background-color:#000;color:#fff;padding:10px}"]
            }] }
];
/** @nocollapse */
AppComponent.ctorParameters = () => [
    { type: Router }
];
if (false) {
    /** @type {?} */
    AppComponent.prototype.routeConfig;
    /** @type {?} */
    AppComponent.prototype.router;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvcmUtYXJjaGl0ZWN0dXJlLWNoaWxkLyIsInNvdXJjZXMiOlsiY2xpZW50L3NyYy9hcHAvYXBwLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQTtBQUNqRCxPQUFPLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQ3ZELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTs7QUFDdkMsYUFBYSxhQUFhLEdBQUcsV0FBVyxDQUFBO0FBU3hDLE1BQU07Ozs7SUFFSixZQUNVO1FBQUEsV0FBTSxHQUFOLE1BQU07UUFFZCxPQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixDQUFDLENBQUE7UUFDdkMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQ2hCLE1BQU0sQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsS0FBSyxZQUFZLGFBQWEsQ0FBQyxDQUNsRCxDQUFDLFNBQVMsQ0FDVCxDQUFDLE1BQXFCLEVBQUUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLEVBQUUsb0NBQW9DLEVBQUUsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQ3hILENBQUE7S0FDRjs7OztJQUVELFFBQVE7UUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLDJCQUEyQixDQUFDLENBQUE7S0FDekM7OztZQXRCRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGFBQWE7Z0JBQ3ZCLFFBQVEsRUFBRTs7R0FFVDs7YUFFRjs7OztZQVZRLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRpb25FbmQgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInXG5pbXBvcnQgeyBmaWx0ZXIgfSBmcm9tICdyeGpzL29wZXJhdG9ycydcbmV4cG9ydCBjb25zdCBST09UX1NFTEVDVE9SID0gJ2NoaWxkLWFwcCdcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBST09UX1NFTEVDVE9SLFxuICB0ZW1wbGF0ZTogYDxwPkNISUxEIC0gQXBwQ29tcG9uZW50PC9wPlxuICAgIDxyb3V0ZXItb3V0bGV0Pjwvcm91dGVyLW91dGxldD5cbiAgYCxcbiAgc3R5bGVVcmxzOiBbJy4vYXBwLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQXBwQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgcHJpdmF0ZSByb3V0ZUNvbmZpZzogYW55XG4gIGNvbnN0cnVjdG9yIChcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyXG4gICkge1xuICAgIGNvbnNvbGUubG9nKCdBcHBNb2R1bGUgLSBBcHBDb21wb25lbnQnKVxuICAgIHJvdXRlci5ldmVudHMucGlwZShcbiAgICAgIGZpbHRlcigoZXZlbnQpID0+IGV2ZW50IGluc3RhbmNlb2YgQWN0aXZhdGlvbkVuZClcbiAgICApLnN1YnNjcmliZShcbiAgICAgIChldmVudHM6IEFjdGl2YXRpb25FbmQpID0+IGNvbnNvbGUubG9nKCclY0N1cnJlbnRSb3V0ZURhdGE6ICcsICdiYWNrZ3JvdW5kLWNvbG9yOmdyZWVuO2NvbG9yOndoaXRlJywgZXZlbnRzLnRvU3RyaW5nKCkpXG4gICAgKVxuICB9XG5cbiAgbmdPbkluaXQgKCkge1xuICAgIGNvbnNvbGUubG9nKCdBbmd1bGFyIEluaXRpYWxpemF0aW9uLi4uJylcbiAgfVxufVxuIl19