import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
export declare const ROOT_SELECTOR = "child-app";
export declare class AppComponent implements OnInit {
    private router;
    private routeConfig;
    constructor(router: Router);
    ngOnInit(): void;
}
