import { OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
export declare const SELECTOR = "app-container";
export declare class AppContainer implements OnInit {
    private activatedroute;
    name: Observable<string>;
    constructor(activatedroute: ActivatedRoute);
    ngOnInit(): void;
}
