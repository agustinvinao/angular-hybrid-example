/**
 * Generated bundle index. Do not edit.
 */
export * from './public_api';
export { AppComponent as ɵb, ROOT_SELECTOR as ɵa } from './client/src/app/app.component';
export { AppContainer as ɵe, SELECTOR as ɵd } from './client/src/app/app.container';
export { AppRoutingModule as ɵc } from './client/src/app/app.routing';
