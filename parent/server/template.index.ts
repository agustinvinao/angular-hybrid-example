import * as fs from 'fs'

export const template = (
  normalizeAssets: any,
  assetsByChunkName: any,
  fs: any,
  outputPath: string,
  metadata: any,
  headTags: Array<any>
) => {
  const normalizedAssetsNamesCSS = Object.keys(assetsByChunkName)
    .reduce((assets, name) => {
      assets.push(normalizeAssets(assetsByChunkName[name])
        .filter(path => path.endsWith('.css')))
      return assets
    }, [])
    .reduce((acc, val) => {
      return acc.concat(val)
    }, []).reverse()
  const normalizedAssetsNamesJS = Object.keys(assetsByChunkName)
    .reduce((assets, name) => {
      assets.push(normalizeAssets(assetsByChunkName[name])
        .filter(path => path.endsWith('.js'))
      )
      return assets
    }, [])
    .reduce((acc, val) => {
      return acc.concat(val)
    }, []).reverse()
  return `
<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${metadata.title}</title>
    <meta name="description" content="${metadata.description}">
    <base href="${metadata.baseUrl}">
    ${headTags && headTags.length ? headTags : ''}
    ${ (metadata.isDevServer && metadata.HMR !== true) ? '<script src="/webpack-dev-server.js" > </script>' : ''}
    <style>
      ${normalizedAssetsNamesCSS
        .filter(path => path.endsWith('.css'))
        .map(path => fs.readFileSync(outputPath + '/' + path))
        .join('\n')
      }
    </style>
  </head>

  <body>

    <app>
      Loading...
    </app>

    ${normalizedAssetsNamesJS.reduce((scripts: Array<any>, name: String) => {
      scripts.push(`<script src="/${name}"></script>`)
      return scripts
    }, []).join('\n')}
  </body>

</html>
`
}
