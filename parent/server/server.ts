const path = require('path')
const express = require('express')
const webpack = require('webpack')
const config = require('../webpack.config.js')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')

const server = express()
const compiler = webpack(config)

server.use(webpackDevMiddleware(compiler))
server.use(webpackHotMiddleware(compiler))

server.get('*', function response (req, res) {
  res.sendFile('index.html', { root: __dirname })
})

server.listen(3000, '0.0.0.0', function onStart (err) {
  if (err) { console.log(err) }
  console.info('==> 🌎 Listening on port %s. Open up http://0.0.0.0:3000/ in your browser.')
})
// // https://itnext.io/building-restful-web-apis-with-node-js-express-mongodb-and-typescript-part-1-2-195bdaf129cf
// // node --inspect -r ts-node/register server/server.ts

// import { App } from './app'
// import * as webpack from 'webpack'
// import * as webpackDevMiddleware from 'webpack-dev-middleware'
// import * as webpackHotMiddleware from 'webpack-hot-middleware'
// import * as config from '../webpack.config'
// // import path from 'path'
// const compiler = webpack(config)
// const server = new App(config, webpackDevMiddleware, compiler)

// server.app.use(
//   webpackDevMiddleware(compiler, {
//     publicPath: config.output.publiPath,
//     noInfo: true
//     // noInfo: true
//     // serverSideRender: true,
//     // index: false,
//     // hot: true,
//     // historyApiFallback: true,
//     // watchOptions: {
//     //   ignored: /node_modules/
//     // }
//   })
// )

// server.app.use(
//   webpackHotMiddleware(compiler, {
//     log: console.log,
//     path: '/__what',
//     heartbeat: 10 * 1000
//   })
// )

// server.setupRoutes(config.output.path)

// const PORT = 3000

// // server.app.get('*', (req, res, next) => {
// //   const filename = path.join(compiler.outputPath, 'index.html')
// //   compiler.outputFileSystem.readFile(filename, function (err, result) {
// //     if (err) {
// //       return next(err)
// //     }
// //     res.set('content-type', 'text/html')
// //     res.send(result)
// //     res.end()
// //   })
// // })

// server.app.listen(PORT, () => {
//   console.log('Express server listening on port ' + PORT)
// })
