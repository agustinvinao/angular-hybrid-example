import { Request, Response, Application } from 'express'
import * as path from 'path'
// import { template } from './template.index'
export class Routes {
  public routes (app: Application, config: any, devMiddleware: any, compiler: any): void {
    // app.route('/').get((req: Request, res: Response) => {
    app.get('/', (req: Request, res: Response) => {
      // res.status(200).send({ message: 'Get Request successfull!' })
      // res.send('hello world')
      console.log('@@@@@@@@@@@@@@@@@@@@@@@@')
      res.sendFile(path.join(__dirname, 'index.html'))
      // const assetsByChunkName = res.locals.webpackStats.toJson().assetsByChunkName
      // const fs = res.locals.fs
      // const outputPath = res.locals.webpackStats.toJson().outputPath
      // res.send(
      //   template(
      //     normalizeAssets,
      //     assetsByChunkName,
      //     fs,
      //     outputPath,
      //     JSON.parse(getPlugin(config, 'DefinePlugin').definitions.metadata),
      //     getPlugin(config, 'HtmlElementsPlugin').locations.headTags
      //   )
      // )

    })

    // app.use('*', function (req, res, next) {
    //   const filename = path.join(compiler.outputPath, 'index.html')
    //   devMiddleware.waitUntilValid(() => {
    //     compiler.outputFileSystem.readFile(filename, (err: any, result: any) => {
    //       if (err) { return next(err) }
    //       res.set('content-type', 'text/html')
    //       res.send(result)
    //       res.end()
    //     })
    //   })
    // })

  }
}

const getPlugin = (config: any, name: String) => {
  return config.plugins.find(
    (plugin: any, index: number) => config.plugins[index].constructor.name === name
  )
}

const normalizeAssets = (assets: Array<any>) => {
  if (typeof assets === 'object') {
    return Object.keys(assets).map(key => assets[key])
  }
  return Array.isArray(assets) ? assets : [assets]
}
