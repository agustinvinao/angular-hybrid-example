import * as express from 'express'
import * as bodyParser from 'body-parser'
import { Routes } from './routes'
import * as path from 'path'

export class App {
  public app: express.Application
  public routePrv: Routes = new Routes()
  private devMiddleware: any
  private compiler: any
  private config: any

  constructor(config: any, devMiddleware: any, compiler: any) {
    this.app = express()
    this.setup()
    this.config = config
    this.devMiddleware = devMiddleware
    this.compiler = compiler
  }

  private setup (): void {
    // support application/json type post data
    this.app.use(bodyParser.json())

    // support application/x-www-form-urlencoded post data
    this.app.use(bodyParser.urlencoded({ extended: false }))
  }

  public setupRoutes (outputPath: string): void {
    // this.app.use(express.static(outputPath))
    // this.routePrv.routes(this.app, this.config, this.devMiddleware, this.compiler)
    console.log('@@@@@@@@@@@@@@ setupRoutes')
    this.app.get('/', (req: express.Request, res: express.Response) => {
      console.log('@@@@@@@@@@@@@@ /')
      res.sendFile(path.join(__dirname, 'index.html'))
    })
  }
}
