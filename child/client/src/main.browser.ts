
/**
 * Angular bootstrapping
 */
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'
import { environment } from '@root/environments/environment'
import { NgModuleRef, NgModule, ApplicationRef, enableProdMode } from '@angular/core'

/**
 * App Module
 * our top level module that holds all of our components
 */
import { AppModule } from './app'
// import { ROOT_SELECTOR, AppComponent } from './app/app.component'

// import { hmrBootstrap } from './hmr'

// if (environment.production) {
//   enableProdMode()
// }

// const bootstrap = () => platformBrowserDynamic().bootstrapModule(AppModule)

// if (environment.hmr) {
//   if (module['hot']) {
//     hmrBootstrap(module, bootstrap);
//   } else {
//     console.error('HMR is not enabled for webpack-dev-server!')
//     console.log('Are you using the --hmr flag for ng serve?')
//   }
// } else {
//   bootstrap().catch(err => console.log(err))
// }

/**
 * Bootstrap our Angular app with a top level NgModule
 */
export function main (): Promise<any> {
  let modulePromise: Promise<NgModuleRef<AppModule>> = null
  if (module['hot']) {
    module['hot'].accept((err: any) => {
      console.log("%c module['hot'].accept - error", 'background-color:red;color:white;', err)
    })
    module['hot'].dispose((data: any) => {
      console.log("%c module['hot'].dispose", 'background-color:red;color:white;')
    })
  }

  modulePromise = platformBrowserDynamic().bootstrapModule(AppModule)

  return modulePromise.then(environment.decorateModuleRef).catch((err) => console.error(err))
}

/**
 * Needed for hmr
 * in prod this is replace for document ready
 */
switch (document.readyState) {
  case 'loading':
    document.addEventListener('DOMContentLoaded', _domReadyHandler, false)
    break
  case 'interactive':
  case 'complete':
  default:
    main()
}

function _domReadyHandler () {
  document.removeEventListener('DOMContentLoaded', _domReadyHandler, false)
  main()
}
