import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { CommonModule } from '@angular/common'
import { AppContainer } from './app.container'

const routes: Routes = [
  {
    path: '',
    redirectTo: 'foo'
  },
  {
    path: 'foo',
    component: AppContainer,
    data: {
      name: 'foo'
    }
  },
  {
    path: 'bar',
    component: AppContainer,
    data: {
      name: 'bar'
    }
  }
]

@NgModule({
  declarations: [AppContainer],
  entryComponents: [AppContainer],
  imports: [
    CommonModule,
    // RouterModule.forRoot(routes, { useHash: true })
    RouterModule.forChild(routes)
  ]
})
export class AppRoutingModule { }
