import { Component, OnInit } from '@angular/core'
import { Router, ActivationEnd } from '@angular/router'
import { filter } from 'rxjs/operators'
export const ROOT_SELECTOR = 'child-app'

@Component({
  selector: ROOT_SELECTOR,
  template: `<p>CHILD - AppComponent</p>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  private routeConfig: any
  constructor (
    private router: Router
  ) {
    console.log('AppModule - AppComponent')
    router.events.pipe(
      filter((event) => event instanceof ActivationEnd)
    ).subscribe(
      (events: ActivationEnd) => console.log('%cCurrentRouteData: ', 'background-color:green;color:white', events.toString())
    )
  }

  ngOnInit () {
    console.log('Angular Initialization...')
  }
}
