import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Observable } from 'rxjs'

export const SELECTOR = 'app-container'

@Component({
  selector: SELECTOR,
  template: `
  <p>CHILD - container</p>
  <ul>
    <li><a [routerLink]="['../foo']">foo</a></li>
    <li><a [routerLink]="['../bar']">bar</a></li>
  </ul>
  <div [className]="name">route: {{name}}</div>`,
  styles: [`
    .foo {
      background-color: red;
      color: white;
      padding: 10px;
    }
    .bar {
      background-color: green;
      color: white;
      padding: 10px;
    }`]
})
export class AppContainer implements OnInit {
  public name: Observable<string>
  constructor (
    private activatedroute: ActivatedRoute
  ) {
    console.log('CHILD - AppContainer - constructor')
    activatedroute.data.subscribe(
      ({ name }) => this.name = name
    )
  }

  ngOnInit () {
    console.log('AppContainer - onInit')
  }
}
