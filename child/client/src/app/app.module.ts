import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'
// import { BrowserModule } from '@angular/platform-browser'
import { AppComponent } from './app.component'
import { AppRoutingModule } from './app.routing'

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent
  ],
  imports: [
    // BrowserModule,
    RouterModule,
    CommonModule,
    AppRoutingModule
  ]
})

export class AppModule {
  constructor () {
    console.log('CHILD - AppModule - constructor')
  }
}
